  @extends('master')
  @section('container')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Games
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#"><i class="fa fa-users"></i> Game Management</a></li>
        <li class="active">Games</li>
      </ol>
	    <!-- Main content -->
    </section>

    <!-- Main content -->
    <section class="content">
     @if(Session::has('flash_message'))
    <div class="add_offer_holder alert {{ Session::get('alert-class') }}">
        <span>{{ Session::get('flash_message') }}</span> <br/>
    </div>
    @endif 
    @if(count($errors->all())>0)
    <div class="alert alert-danger">
        @foreach($errors->all() as $err)
        {{$err}}<br/>
        @endforeach
    </div>
    @endif
    
      <div class="row">
        <div class="col-xs-19">
          <div class="box">
            <div class="box-header">
             
            
            <!-- /.box-header -->
            <div class="box-body">
				<a class="btn btn-info" id="btn_add_new"><i class="fa fa-plus"></i> Add new</a><br/>
			<table>
			 <div class="box-body">
              <table id="example1" class="table table-bordered table-striped" id="tbl_roles">
    <thead>
        <tr>
			<th>#</th>
            <th>User</th>
            <th>Level</th>
            <th>Step</th>
            <th>Progress</th>
            <th>Time</th>
            <th>Status</th>
            <th colspan='4'>Action</th>
        </tr>
    </thead>
    <tbody>
        <!--Use a while loop to make a table row for every DB row-->
        <?php $x = 1;?>
		  @foreach( $gamesResults as $game )
                        <tr>
                            <td> {{ $x }}</td>
                            <td> {{ $game->lastname }}, {{ $game->firstname }}</td>
                            <td> {{ $game->level }}</td>
                            <td> {{ $game->step }}</td>
                            <td> {{ $game->process }}</td>
                            <td> {{ $game->gameTime }} </td>
                            <td> {{ $game->status }}</td>
                            <td> <a href="javascript:void(0)" id="btn-edit-role" data-id="{{ $game->id }}" class='btn btn-primary btn-edit-role'><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
							</td>
           <?php $x++; ?>
         @endforeach
       
    </tbody>
</table>

                </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      </div>
      <!-- /.row -->
    </section>
     <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form role="form" method="post" action="" id="form_roles" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">View/Edit</h4>
                    </div>
                    <div class="modal-body">  
                        <input type="hidden" name="id" id="role_id" value="new" />        
                        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" /> 
                        <div class="form-group">
                            <label for="type_name">Name</label>
                            <input type="text" name="name" id="name" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="type_name">Description</label>
                            <textarea name="description" id="description" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="type_name">Image</label>
                            <input type="file" name="image" id="image" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_close_modal">
                            <i class="glyphicon glyphicon-remove"></i> Close
                        </button>
                        <button type="submit" name="submit" class="btn btn-success" id="btn_save_changes">
                            <i class="glyphicon glyphicon-ok"></i> Save changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
@stop
@section('scripts')
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
	<script src="{{ asset('js/admin/games.js') }}"></script>
@stop