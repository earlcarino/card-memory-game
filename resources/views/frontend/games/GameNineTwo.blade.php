@extends('masterFrontend')

@section('title', 'Memory Test')
@section('styles')
<!--styles here-->
<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
@stop

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@stop

@section('content')
    <div class="" id="app">
        <gameninetwo></gameninetwo>
    </div>
@stop

@section('scripts')
    <script type="application/javascript" src="{{ asset('js/app.js') }}"></script>
@stop