<html>
    <head>
        <title>Lift Brain Training - @yield('title')</title>
        @include('_partials.frontend.styles')
        @yield('styles')
        <script>
            window.Laravel = { "csrfToken": "{{csrf_token()}}" };
        </script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        @yield('content')
        @include('_partials.frontend.scripts')
        @yield('scripts')
    </body>
</html>