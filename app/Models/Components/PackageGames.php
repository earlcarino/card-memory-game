<?php

namespace App\Models\Components;

// use Illuminate\Database\Eloquent\Model;
use DB;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;

class PackageGames extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "subscription_package";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
		'package_id',
    	'game_id'
    ];

    protected $guarded = ['id'];
    
     public function getPackageGames($id){
        $data = DB::table('package_games')
        ->join('games','games.id','package_games.game_id')
        ->select(DB::raw("games.*"))
        ->where('games.status','=',1)
        ->where('package_games.package_id','=',$id)
        ->get();
        return $data;
    }

}
