<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $event_count = 0;
        $awaitdeposit_count = 0;
        $nextweek_count = 0;
        
        $data = array(
            'dashboard_menu'=> 1,
        );
        return view('admin.dashboard');
    }
}
