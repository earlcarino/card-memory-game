<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use App\Models\Components\UserRoles;

class UserRolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = UserRoles::where('active','=',1)->get();
		
		$data = array(
			'user_type' => $type
		);
		return view('admin.users.user_roles.index')->with($data);
    }

    private function validator(Request $request)
    {
        $validator = Validator::make($request->except('_token'), [
            'id' => 'required',
            'name' => 'required|unique:roles'
        ]);
        
        return $validator;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request);
        if ($validator->fails()) {
           \Session::flash('flash_message', $validator->errors());
			\Session::flash('alert-class', 'alert-danger');
	
			return response()->json(['success'=>'false']);
        }
        
        $user_type = new UserRoles;
        $user_type->name = Input::get('name');
        if ($user_type->save()) {
          	\Session::flash('flash_message', 'User Role was added to your list!');
    		\Session::flash('alert-class', 'alert-success');
    	
    		return response()->json(['success'=>'true']);
        }
        	\Session::flash('flash_message', 'There is an error while saving the data!');
    		\Session::flash('alert-class', 'alert-danger');
    	
    		return response()->json(['success'=>'false']);
    }

    public function edit($id){
    	$user_type = UserRoles::find($id);
    	return response()->json(['success'=>'true','data' => $user_type]);
    } 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->validator($request);
        if ($validator->fails()) {
        	\Session::flash('flash_message', $validator->errors());
        	\Session::flash('alert-class', 'alert-danger');
        	 
        	return response()->json(['success'=>'false']);
        }

        $id = Input::get('id');
        
        $user_type = UserRoles::find($id);
        $user_type->name = Input::get('name');
      

        if ($user_type->save()) {
           \Session::flash('flash_message', 'User Role was updated successfully!');
    		\Session::flash('alert-class', 'alert-success');
    	
    		return response()->json(['success'=>'true']);
        }
        	\Session::flash('flash_message', 'There is an error while saving the data!');
    		\Session::flash('alert-class', 'alert-danger');
    	
    		return response()->json(['success'=>'false']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Components\UserType  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$deleteItem = UserRoles::find($id);
    	$deleteItem->active = 0;
    	$deleteItem->save();
    	
    	\Session::flash('flash_message', 'User Role was deactivated in your list!');
    	\Session::flash('alert-class', 'alert-danger');
    	
    	return redirect()->back();
    }
}
