<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Models\Components\Packages;
use App\Models\Components\PackageGames;

class PackageController extends Controller
{
	public function index(){
	//view all users	
		$packages = Packages::get();
		$data = array(
				'packages' => $packages
		);
		return view('admin.package.index')->with($data);
	}
	public function activate($id)
	{
		$deleteItem = Packages::find($id);
		$deleteItem->status = 1;
		$deleteItem->save();
			
		\Session::flash('flash_message', 'User was activated in your list!');
		\Session::flash('alert-class', 'alert-success');
			
		return redirect()->back();
	}
	
	public function deactivate($id)
	{
		$deleteItem = Packages::find($id);
		$deleteItem->status = 0;
		$deleteItem->save();
			
		\Session::flash('flash_message', 'User was deactivated in your list!');
		\Session::flash('alert-class', 'alert-danger');
			
		return redirect()->back();
	}
	
	private function validator(Request $request)
	{
		$validator = Validator::make($request->except('_token'), [
				'id' => 'required',
				'package_name' => 'required',
				'monthly_fee' => 'required',
				'annual_fee' => 'required'
		]);
	
		return $validator;
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = $this->validator($request);
		if ($validator->fails()) {
			\Session::flash('flash_message', $validator->errors());
			\Session::flash('alert-class', 'alert-danger');
	
			return response()->json(['success'=>'false']);
		}
		$package = new Packages($request->except('_token'));;
		$package->status = 1;

		if ($package->save()) {
			\Session::flash('flash_message', 'Package was added to your list!');
			\Session::flash('alert-class', 'alert-success');
			 
			return response()->json(['success'=>'true']);
		}
		\Session::flash('flash_message', 'There is an error while saving the data!');
		\Session::flash('alert-class', 'alert-danger');
		 
		return response()->json(['success'=>'false']);
	}
	
	public function edit($id){
		$user = Packages::find($id);
		return response()->json(['success'=>'true','data' => $user]);
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$id = Input::get('id');
			$validator = Validator::make($request->except('_token'), [
				'id' => 'required',
				'package_name' => 'required',
				'monthly_fee' => 'required',
				'annual_fee' => 'required'
		]);
		if ($validator->fails()) {
			\Session::flash('flash_message', $validator->errors());
			\Session::flash('alert-class', 'alert-danger');
	
			return response()->json(['success'=>'false']);
		}

		$package = Packages::find($id);
		$package->package_name = Input::get('package_name');
		$package->description = Input::get('description');
		$package->monthly_fee = Input::get('monthly_fee');
		$package->annual_fee = Input::get('annual_fee');
	
		if ($package->save()) {
			\Session::flash('flash_message', 'Package was updated successfully!');
			\Session::flash('alert-class', 'alert-success');
			 
			return response()->json(['success'=>'true']);
		}
		\Session::flash('flash_message', 'There is an error while saving the data!');
		\Session::flash('alert-class', 'alert-danger');
		 
		return response()->json(['success'=>'false']);
	}

	
}