<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Models\Components\GameResults;
use App\Models\Components\Users;

class GameResultsController extends Controller
{
	public function index(){
	//view all users	
		$gamesResults = GameResults::get();
		//$userList = Users::get();
        //$user = Users::find($gamesResults->userId);
        $content;
        $x = 0; 
        foreach($gamesResults as $game) {
            $user = Users::find($game->userId);
            $gamesResults[$x]->firstname = $user->firstname;
            $gamesResults[$x]->lastname = $user->lastname;
            $x++;
        }
		$data = array(
				'gamesResults' => $gamesResults
		);
        
		return view('admin.games.results.index')->with($data);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
        $user = auth()->user();
        $gameResultId = GameResults::where('userId',"=",$user->id)
                        ->first(['id']);
        $gameResultCount = GameResults::where('userId',"=",$user->id)
                        ->count();
        
        if($gameResultCount === 0) {
            $game_results           = new GameResults;
            $game_results->userId   = $user->id;
            $game_results->level    = $request->data['level'];
            $game_results->step     = $request->data['step'];
            $game_results->process  = $request->data['process'];
            $game_results->status   = $request->data['status'];
            $game_results->life     = count(array_filter(($request->data['life'])));
            $game_results->gameTime = $request->data['gameTime'];
            $game_results->status   = $request->data['status'];
            $message                = "add";
        } else {
            $game_results           = GameResults::find($gameResultId->id);
            $game_results->userId   = $user->id;
            $game_results->level    = $request->data['level'];
            $game_results->step     = $request->data['step'];
            $game_results->process  = $request->data['process'];
            $game_results->status   = $request->data['status'];
            $game_results->life     = count(array_filter(($request->data['life'])));
            $game_results->gameTime = $request->data['gameTime'];
            $game_results->status   = $request->data['status'];
            $message                = "update";
        }
        
        if($game_results->save()){
            return response()->json(['success'=>'true','data'=>$message]);
        } else {
            return response()->json(['success'=>'false']);
        }
	}
	
	public function edit($id){
		
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		
	}

}