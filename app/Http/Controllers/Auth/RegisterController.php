<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Models\Components\Users;
use App\Models\Components\Individuals;
use App\Models\Components\Partners;
use App\Models\Components\Packages;
use App\Models\Components\PackageGames;

use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\AdaptivePayments;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
    
    public function index(){
    	
    	$token_code = str_random(50);
    	$data = array(
    			'token_code' => $token_code
    	);
    	 
    	return view('auth.registration')->with($data);
    }

     public function registerPartner(){
        
        $token_code = str_random(50);
        $partner = Partners::where('approved','=',1)->get();
        $data = array(
            'token_code' => $token_code,
            'partners' => $partner
        );
        
        return view('auth.registerpartner')->with($data);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    
    public function storeSeller(Request $request)
    {
    
    	$validator = Validator::make($request->except('_token'), [
    			'username' => 'required|unique:users,username,',
    			'firstname' => 'required|regex:/^[A-Za-z\s-_]+$/',
    			'lastname' => 'required|regex:/^[A-Za-z\s-_]+$/',
    			'email_address' => 'required|email|unique:users,email_address,',
    			'token_code' => 'required|unique:users,token_code,',
    			'password' => 'required',
    			'status' => 'required',
    			'user_role' => 'required',
    			'confirm_password' => 'required|same:password'
    	]);
    
    	$request['password'] = bcrypt($request['password']);
    	$users = new Users($request->except('_token'));
    	
    	//Original script - START
    	//if (!($validator->fails()) && $users->save()) {
    	if ((!$validator->fails()) && $users->save()) {
    		
    		
    		try {
    			$user = Auth::user();
    			 
    			//check if registration is a buyer or seller
    			if($request['user_role'] == 2) {
    				///if subscribers

    				return redirect('/')
    				->with('message', 'Your account was created successfully. Please check your email for verification.')
    				->withErrors($validator);
    			} else {
    
    				return redirect('/')
    				->with('message', 'Your account was created successfully. We will send your verification link once your registration is approved.')
    				->withErrors($validator);
    			}
    			 
    		} catch (\Exception $e) {
    			\Session::flash('flash_message', "Failed sending email verification.");
    			\Session::flash('alert-class', 'alert-danger');
    			return redirect()->back();
    		}
    	} elseif ($validator->fails()) {
    		return redirect()->back()
    		->withErrors($validator)->with('error', 'Error')
    		->withInput();
    	}
    	//Original script - END
    }
    
    public function resendEmail()
    {
    	return view('auth.resendemail');
    }

     public function selectPackage()
    {
        $package_model = new Packages();
        $packages = $package_model->displayPackage();
        
        $package_game_model = new PackageGames();
        
        foreach($packages as $package) {
            $package->games = $package_game_model->getPackageGames($package->id);
        }
       // dd($packages);
    //    exit;
        $data = array(
            'packages' => $packages
        );
        return view('auth.packages')->with($data);
    }
    
    public function checkout(Request $request){
        
    }
}
