<?php

namespace App\Http\Controllers\Auth;

use App\Models\Components\Users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function index(){
    	return view('auth.login');
    }
    
    public function login(Request $request)
    {
    	$rules = array(
    			'username' => 'required', // make sure the email is an actual email
    			'password' => 'required'
    	); //required|alphaNum|min:8'
    	 
    	$validator = Validator::make(Input::all(), $rules);
    	if ($validator->fails()) {
    		return redirect('/login')->withErrors($validator) // send back all errors to the login form
    		->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
    	} else {
    		$username = Input::get('username');
    		$password = Input::get('password');
    		 
    		$userdata = array(
    				'username' => $username,
    				'password' => $password
    				 
    		);
    		 
    		if (Auth::attempt($userdata)) {
    			// validation successful
    			$user_id = Auth::user()->id;
    			//get user status
    			$user = Users::find($user_id);
    			//dd($user->status);
    			//check if account was validated
    			 
    			if ($user->status == 1) {
    				//active user
    				return redirect('/dashboard');
    			} else {
    				Auth::logout();
    				return  back()->withErrors('Please validate your email address to login.');
    			}
    			 
    		} else {
    			//echo "incorrect";
    			return redirect('/login')->withErrors('Username/Password is incorrect.');
    		}
    	}
    }
}
