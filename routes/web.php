<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Auth\LoginController@index');
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@index']);
Route::post('/login-user', 'Auth\LoginController@login');
Route::get('/registration/{package}', 'Auth\RegisterController@index');
Route::get('/register-partner', 'Auth\RegisterController@registerPartner');
Route::post('/register-user', 'Auth\RegisterController@storeUser');

Route::get('/forgot-password', 'Auth\ForgotPasswordController@index');
Route::get('/resend-email', 'Auth\RegisterController@resendEmail');
Route::post('/resend-email', 'Auth\RegisterController@sendVerificationEmail');
Route::post('/reset-my-password', 'Auth\ForgotPasswordController@resetMyPassword');
Route::get('/update-user-password/{token_code}/', 'Auth\ForgotPasswordController@loadUpdatePassword');
Route::post('/update-password', 'Auth\ForgotPasswordController@updateNewPassword');

//select package
Route::get('/select-package', 'Auth\RegisterController@selectPackage');
//checkout
Route::post('/checkout', 'Auth\RegisterController@checkout');
//verify account
Route::get('/register/verify-user-account/{id}/{code}', 'Auth\RegisterController@verifyAccount');

//frontend
Route::get('gameplay/one', ['uses' => 'Frontend\GamesController@gameOne']);

/* Routes shared by admin, seller, and buyer */
Route::group(['middleware' => 'auth'], function () {



Route::get('/edit-profile', 'admin\UsersController@editProfile');
Route::post('/update-profile', 'admin\UsersController@updateProfile');

//Dashboard
Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'Admin\DashboardController@index']);

//User Roles
Route::get('roles', ['as' => 'roles', 'uses' => 'Admin\UserRolesController@index']);
Route::post('/roles/store', 'Admin\UserRolesController@store');
Route::delete('/roles/delete/{id}', 'Admin\UserRolesController@destroy');
Route::patch('/roles/update', 'Admin\UserRolesController@update');
Route::get('/roles/{role}', 'Admin\UserRolesController@edit');

//Users
Route::get('users', ['as' => 'users', 'uses' => 'Admin\UsersController@index']);
Route::post('/users/store', 'admin\UsersController@store');
Route::post('/users/deactivate/{id}', 'admin\UsersController@deactivate');
Route::post('/users/activate/{id}', 'admin\UsersController@activate');
Route::patch('/users/update', 'admin\UsersController@update');
Route::get('/users/{role}', 'admin\UsersController@edit');

//Games
Route::get('games', ['as' => 'games', 'uses' => 'Admin\GamesController@index']);
Route::post('/games/store', 'admin\GamesController@store');
Route::post('/games/deactivate/{id}', 'admin\GamesController@deactivate');
Route::post('/games/activate/{id}', 'admin\GamesController@activate');
Route::patch('/games/update', 'admin\GamesController@update');
Route::get('/games/{role}', 'admin\GamesController@edit');
Route::post('gameplay/api/result/store', 'admin\GameResultsController@store');


//Games Results
Route::get('game/results/', ['as' => 'gameResults', 'uses' => 'Admin\GameResultsController@index']);

//Packages
Route::get('packages', ['as' => 'packages', 'uses' => 'Admin\PackageController@index']);
Route::post('/packages/store', 'admin\PackageController@store');
Route::post('/packages/deactivate/{id}', 'admin\PackageController@deactivate');
Route::post('/packages/activate/{id}', 'admin\PackageController@activate');
Route::patch('/packages/update', 'admin\PackageController@update');
Route::get('/packages/{role}', 'admin\PackageController@edit');


//Partners
Route::get('partners', ['as' => 'partners', 'uses' => 'Admin\PartnersController@index']);
//Subscribers
Route::get('subscribers', ['as' => 'subscribers', 'uses' => 'Admin\SubscribersController@index']);
});

Route::get('/logout', function () {
	Auth::logout();
	return redirect('/');
});
