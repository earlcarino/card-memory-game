	$(function (){
		var myModal = $("#myModal"),			
		btn_add_new = $("#btn_add_new"),
		btn_save_changes = $("#btn_save_changes"),
		btn_close_modal = $("#btn_close_modal"),
		myModalLabel = $("#myModalLabel"),
		form_roles = $("#form_roles"),
		firstname = $("#firstname"),
		middlename = $("#middlename"),
		lastname = $("#lastname"),
		username = $("#username"),
		password = $("#password"),
		user_type = $("#user_role"),
		email_address = $("#email_address"),
		role_id = $("#role_id");
		
		
		btn_add_new.click(function (){
			myModalLabel.html("Add New Back-End User");
			myModal.modal('show'); 
			role_id.val("new");
		});
		
		$(document).on("click", ".btn-edit-role", function (){
			var $this = $(this), 
				id = $this.attr("data-id");
				
			myModalLabel.html("View/Edit Back-End User");	
			role_id.val(id);
			
			password.removeAttr('required');
			password.attr("placeholder","Enter New Password here");
			$.ajax({
				url: '/users/'+id,
				type: 'get',
				dataType: 'json',
				success: function (result){
					var res = result.data;
						firstname.val( res.firstname );
						middlename.val( res.middle_initial );
						lastname.val( res.lastname );
						username.val( res.username );
						user_type.val( res.user_type_id );
						email_address.val( res.email_address );
						role_id.val( res.id );
						myModal.modal('show');
				
				}
			});
			myModal.modal('show');
		});
		
		form_roles.on('submit', function (e){
			var $this = $(this),
				requestType = 'patch',
				action = 'update';

			e.preventDefault();

			if( role_id.val() == 'new' ){
				requestType = 'post';
				action = 'store';
			}

			$.ajax({
				url: '/users/'+action,
				type: requestType,
				dataType: 'json',
				data: $this.serialize(),
				beforeSend: function (){
					btn_save_changes.addClass("disabled").attr("disabled", "disabled");
				},
				success: function (data){
					myModal.modal('show'); 
					location.reload(); // then reload the page.(3)
				},
				complete: function (){
					btn_save_changes.removeClass("disabled").removeAttr("disabled");
				}
			});
		});
		
	

});